import csv
import os
import datetime
import ConfigParser
from server_handler import copy_file_to_server, check_if_file_exist

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

path_merger_file = config.get('paths', 'path_merger_file')
path_temp_files_on_windows = config.get('paths', 'path_temp_files_on_windows')
path_user_log_files = config.get('paths', 'path_user_log_files')
path_save_chart_on_storage = config.get('paths', 'path_save_chart_on_storage')
path_save_videos_merged_chart = config.get('paths', 'path_save_videos_merged_chart')

def create_merger_file(video_name, chart_list ):

    file_path = os.getcwd() + path_temp_files_on_windows + "/" + video_name + ".csv"

    with open(file_path, 'wb') as csv_file:
        csvWriter = csv.writer(csv_file, delimiter=',')
        for line in chart_list:
            try:
                if line[0] != "":
                    csvWriter.writerow(line)

            except:
                pass

    return file_path


def id_exist_merger_file(video_name, track_id):

    my_file = path_merger_file + "/" + video_name + ".csv"
    with open(my_file) as infile:
        r = csv.DictReader(infile, skipinitialspace=True)
        for row in r:
            for val in row.itervalues():
                if str(val) == str(track_id):
                    return True
    return False

def create_user_log_file(ssh, user_name):
    file_path = os.getcwd() + path_temp_files_on_windows + "/" + user_name + ".csv"

    with open(file_path, 'wb') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=["video_name", "complexity", "work_time", "finish"])
        writer.writeheader()

    copy_file_to_server(ssh, file_path, path_user_log_files + "/" + user_name +".csv")
    return file_path

def add_video_finished_to_chart(ssh, video_name):
    file_path = os.getcwd() + path_temp_files_on_windows + "/videos_merged.csv"
    remote_dir = path_save_videos_merged_chart + "videos_merged.csv"
    if not check_if_file_exist(ssh, remote_dir):
        with open(file_path, 'wb') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=["video_name", "date"])
            writer.writeheader()
    else:
        sftp = ssh.open_sftp()
        sftp.get(remote_dir, file_path)
        sftp.close()

    with open(file_path, 'a') as csv_file:
        today = datetime.datetime.today().strftime('%Y-%m-%d')
        csv_writer = csv.DictWriter(csv_file, fieldnames=["video_name", "date"])
        csv_writer.writerow({'video_name': video_name, 'date': today})

    copy_file_to_server(ssh, file_path, remote_dir)


def add_user_info(ssh, user_name, video_name, complexity, work_time, finish_bol):
    file_path = os.getcwd() + path_temp_files_on_windows + "/" + user_name + ".csv"
    remote_dir = path_user_log_files + "/" + user_name + ".csv"

    sftp = ssh.open_sftp()
    sftp.get(remote_dir, file_path)
    sftp.close()

    with open(file_path, 'a') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=["video_name", "complexity", "work_time", "finish"])
        writer.writerow({'video_name': video_name, 'complexity': complexity, 'work_time': work_time, 'finish': finish_bol})

    copy_file_to_server(ssh, file_path, path_user_log_files + "/" + user_name +".csv")

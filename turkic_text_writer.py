import ConfigParser
import os

from server_handler import copy_file_to_server

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

path_merger_file = config.get('paths', 'path_merger_file')
path_save_chart_on_storage = config.get('paths', 'path_save_chart_on_storage')

path_temp_files_on_windows = config.get('paths', 'path_temp_files_on_windows')

from line_type import Line


def make_txt_lines(txt, segment_length):
    track_start_segment = 1
    track_id = "-1"
    txt_lines = []
    lines = txt.split('\n')
    for i in range(0, len(lines) - 1):
        line = lines[i].split(' ')

        if int(line[5]) % segment_length == 0 and track_id != line[0]:
            track_start_segment = (int(line[5]) / segment_length) + 1
            track_id = line[0]

        txt_lines.append(
            Line(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8], line[9],
                 line[10:], int(track_start_segment)))
    return txt_lines


def save_merged_turkic_txt_file(ssh, merge_chart, txt_lines, local_image_path, path_on_server, video_name):
    merged_txt_lines = []
    assigned_set = set([])
    path = local_image_path +".txt"

    file_path = os.getcwd() + path_temp_files_on_windows + "/" + video_name + ".csv"
    copy_file_to_server(ssh, file_path, path_save_chart_on_storage + video_name + ".csv")

    for i in range(1, len(merge_chart)):
        merge_line = merge_chart[i]
        try:
            merge_line = filter(lambda a: a != "-5", merge_line)
        except:
            pass
        if len(merge_line) == 1:
            break
        #print merge_line

        main_id = merge_line[0]
        for g in range(len(txt_lines) - 1):
            line = txt_lines[g]
            if line.track_id in merge_line:
                double = False
                for duplicated_line in range(len(txt_lines) - 1):
                    if txt_lines[duplicated_line].frame == line.frame and txt_lines[duplicated_line].track_id in merge_line and txt_lines[duplicated_line].track_id != line.track_id:
                        double = True
                        if duplicated_line > g:
                            line.track_id = main_id
                            merged_txt_lines.append(line)
                        break
                if not double:
                    line.track_id = main_id
                    merged_txt_lines.append(line)

    for line in merge_chart:
        for track in line:
            try:
                assigned_set.add(int(track))
            except:
                pass
    #print "assigned_set "  + str(assigned_set)
    for i in range(len(txt_lines) - 1):
        line = txt_lines[i]
        if not assigned_set.__contains__(int(line.track_id)):
            merged_txt_lines.append(line)

    fo = open(path, "wb")
    for item in merged_txt_lines:
        try:
            fo.write("%s\n" % item.get_turkic_line_str())
        except:
            print item
    fo.close()

    copy_file_to_server(ssh, path, path_on_server)
    return True

import os
import sys
import logging
from sys import exit

'''import sshtunnel
from sshtunnel import SSHTunnelForwarder
import paramiko
import Tkinter
import MySQLdb
'''

from image_handler import import_video, download_dir
from cmd import create_all_folders, turkic_video, merge_video, delete_temp_files
from logic_and_display import run_presentation, set_up_presentation, get_merge_chart, get_delete_files_bol
from datetime import datetime
import time


from server_handler import open_file_on_server, connect_to_server
from sql_handler import get_complexity, find_segment_length_and_gap

from turkic_text_writer import save_merged_turkic_txt_file, make_txt_lines
from user_handler import log_in_user

import ConfigParser
config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

logging_level = config.get('logging', 'logging_level')
path_save_turkic_files = config.get('paths', 'path_save_turkic_files')
path_merger_file = config.get('paths', 'path_merger_file')
path_videos_on_storage = config.get('paths', 'path_videos_on_storage')
path_merged_turkic_files = config.get('paths', 'path_merged_turkic_files')
path_images_on_storage = config.get('paths', 'path_images_on_storage')

path_temp_files_on_windows = config.get('paths', 'path_temp_files_on_windows')

date = str(datetime.today().strftime('%d-%m-%y'))


def run(video_name):
    print "starting merger UI"
    ssh = connect_to_server()
    create_all_folders(ssh, date, video_name)

    user = log_in_user(ssh)
    #user = "boaz"

    complexity = get_complexity(video_name)
    #complexity = 1

    video_path = path_videos_on_storage + "/" + video_name + ".mp4"
    local_temp_files = os.getcwd() + path_temp_files_on_windows
    if not os.path.exists(local_temp_files + "/" + video_name + ".mp4"):
        print "importing video"
        try:
            import_video(ssh, video_path, local_temp_files, video_name)
        except:
            logging.warning("video does not exist on server: %s" % video_path)

        print "turkic video"
        turkic_video(ssh, video_name)

    video_path = local_temp_files + "/" + video_name + ".mp4"

    image_path = path_images_on_storage + "/" + video_name + "/"
    local_image_path = local_temp_files + "/" + video_name
    if not os.path.exists(local_image_path + "/storage\ext_media\groundtruth\stripping/" + video_name):
        print "importing images"
        download_dir(ssh, image_path, local_image_path, video_name)
        time.sleep(2)

    segment_length, gap = find_segment_length_and_gap(video_name)
    #segment_length =321
    #gap = 21

    txt = open_file_on_server(ssh, path_save_turkic_files, video_name + ".txt")
    txt_lines = make_txt_lines(txt.read(), segment_length)
    if int(txt_lines[-1].frame) <= segment_length:
        print "video have only 1 segment, exiting"
        exit()

    print "merging video '%s'" %video_name

    merge_video(ssh, video_name, False)
    merge_chart = open_file_on_server(ssh, path_merger_file, video_name + ".csv")
    time.sleep(2)

    print "setting up presentation"
    set_up_presentation(ssh, txt_lines, merge_chart, video_name, local_image_path, video_path, user, complexity, segment_length, gap)
    run_presentation()

    print "good work"
    ssh.close()


logname = "merger_UI_log.log"
logging.basicConfig(filename=logname,
                            filemode='w',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=int(logging_level))
#attribute = str(raw_input("enter video name "))
#run(attribute)
if len(sys.argv) < 2:
    print "enter video name"
    exit()
run(sys.argv[1])


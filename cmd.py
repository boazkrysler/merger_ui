
import subprocess
import logging
import os
import ConfigParser
import shutil

from server_handler import check_if_file_exist, run_command_on_server

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
path_save_turkic_files = config.get('paths', 'path_save_turkic_files')
path_merger_file = config.get('paths', 'path_merger_file')
path_merged_turkic_files = config.get('paths', 'path_merged_turkic_files')
path_save_chart_on_storage = config.get('paths', 'path_save_chart_on_storage')
path_temp_files_on_windows = config.get('paths', 'path_temp_files_on_windows')
path_save_finish_chart_on_storage = config.get('paths', 'path_save_finish_chart_on_storage')

path_merger_script = config.get('paths', 'path_merger_script')

path_user_log_files = config.get('paths', 'path_user_log_files')

def merge_video(ssh, video_name, force):
    if force or not check_if_file_exist(ssh, path_merger_file + "/" + video_name + ".csv"):
        logging.debug("merge_video, creating new chart")
        command = "cd " + path_merger_script + "; python video_merger.py " + video_name + " 1 " + path_save_turkic_files
        run_command_on_server(ssh, command)
    else:
        logging.debug("merge_video, chart already exist")


def turkic_video(ssh, video_name):
    logging.debug("turkic_video: %s" %video_name)

    command = "cd /root/vatic ; sudo turkic dump " + video_name + " -o " + path_save_turkic_files + "/" + video_name + ".txt"
    run_command_on_server(ssh,command)
    return path_save_turkic_files


def create_folder(path):
    logging.debug("create_folder: %s" %path)
    if not os.path.exists(path):
        os.mkdir(path)


def copy_file(ssh, file_path, new_path):
    logging.debug("copy_file: %s" % file_path)
    command = "sudo cp " + file_path + " " + new_path

    run_command_on_server(ssh, command)


def create_folder_on_server(ssh, path):
    logging.debug("create_folder_on_server: %s" %path)
    command = "sudo mkdir " + path
    run_command_on_server(ssh, command)

    command = "sudo chmod -R 777 " + path
    run_command_on_server(ssh, command)


def chmod_path(path):
    logging.debug("chmod_path: %s" %path)

    cmdLine = "sudo chmod -R 777 " + path + "/"
    subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)


def create_all_folders(ssh, date, video_name):
    cwd = os.getcwd()
    create_folder(cwd + path_temp_files_on_windows)
    create_folder(cwd + path_temp_files_on_windows + "/" + video_name)

    create_folder_on_server(ssh, "/tmp")
    create_folder_on_server(ssh, path_save_turkic_files)
    create_folder_on_server(ssh, path_merger_file)
    create_folder_on_server(ssh, path_merged_turkic_files)
    create_folder_on_server(ssh, path_save_chart_on_storage)
    create_folder_on_server(ssh, path_user_log_files)

    create_folder_on_server(ssh, path_save_finish_chart_on_storage)


def delete_temp_files(path):
    shutil.rmtree(path)

import logging
import ConfigParser
import os
import tarfile

import time

from PIL import ImageDraw
from PIL import ImageFont

from server_handler import run_command_on_server

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
path_images_on_storage = config.get('paths', 'path_images_on_storage')
path_save_archive = config.get('paths', 'path_save_archive')
path_images_inside_archive = config.get('paths', 'path_images_inside_archive')
path_temp_files_on_windows = config.get('paths', 'path_temp_files_on_windows')


def get_image_path(max_frame, video_name, frame):
    end_of_video = False
    if frame > max_frame:
        frame = max_frame
        end_of_video = True
    frame_folder = str(int(int(frame) / 100))
    dad_frame_folder = str(int(int(frame) / 10000))
    image_path = os.getcwd() + path_temp_files_on_windows + "/" + video_name + "/" + path_images_inside_archive \
                 + "/" + video_name + "/" + dad_frame_folder + "/" + frame_folder + "/" + str(frame) + ".jpg"
    return image_path, end_of_video


def import_video(ssh,remote_dir, local_dir, video_name):
    logging.debug("import_video: %s" %video_name)
    sftp = ssh.open_sftp()
    os.path.exists(local_dir) or os.makedirs(local_dir)
    sftp.get(remote_dir, local_dir + "/" + video_name + ".mp4")
    return local_dir + "/" + video_name + ".mp4"


def download_dir(ssh,remote_dir, local_dir, video_name):
    logging.debug("download_dir: create zip on server")
    local_path = os.path.join(local_dir, video_name + ".tar.gz")
    run_command_on_server(ssh, "cd " + path_save_archive + "; tar -czvf " + video_name + ".tar.gz " + remote_dir)

    sftp = ssh.open_sftp()
    os.path.exists(local_dir) or os.makedirs(local_dir)
    sftp.get(path_save_archive + "/" + video_name + ".tar.gz", local_path)

    logging.debug("download_dir: extract zip")

    tar = tarfile.open(local_path, "r:gz")
    tar.extractall(path=local_dir)
    tar.close()

    '''
    with tarfile.open("sample.tar") as tar:
    subdir_and_files = [
        tarinfo for tarinfo in tar.getmembers()
        if tarinfo.name.startswith("subfolder/")
    ]
    tar.extractall(members=subdir_and_files)
    '''

def find_if_exist_on_merge_chart(track_id, merge_chart):
    for i in range(1, len(merge_chart)):
        for j in range(len(merge_chart[i])):
            if str(merge_chart[i][j]) == str(track_id):
                return True
    return False

def find_if_merged(track_id_to_print, merge_chart):
    for i in range(1, len(merge_chart)):
        for j in range(len(merge_chart[i])):
            if str(merge_chart[i][j]) == str(track_id_to_print):
                try:
                    for g in range(len(merge_chart[i])):
                        if merge_chart[i][g] != "-5":
                            track_id_to_print = merge_chart[i][g]
                            break
                    return track_id_to_print
                except:
                    return track_id_to_print
    return track_id_to_print


def draw_on_image(image, main_track_id, txt_lines, frame, merge_chart, edit_mode_bol, unmerged_mode_bol, font_size):
    for i in range(len(txt_lines) - 1):
        if txt_lines[i].frame == str(frame):
            track_id_to_print = find_if_merged(txt_lines[i].track_id, merge_chart)
            main_track_bol = False
            color = "blue"
            if int(track_id_to_print) == int(main_track_id):
                main_track_bol = True
                color = "red"

            if edit_mode_bol and int(main_track_id) != int(track_id_to_print):
                image = add_BB(image, track_id_to_print, txt_lines[i].x_min, txt_lines[i].x_max,
                       txt_lines[i].y_min, txt_lines[i].y_max, color, main_track_bol, font_size)
            elif not edit_mode_bol and not unmerged_mode_bol and int(main_track_id) == int(track_id_to_print):
                if txt_lines[i].lost == "0":
                    image = add_BB(image, track_id_to_print, txt_lines[i].x_min, txt_lines[i].x_max,
                                   txt_lines[i].y_min, txt_lines[i].y_max, color, main_track_bol, font_size)
            elif unmerged_mode_bol:
                if not find_if_exist_on_merge_chart(txt_lines[i].track_id, merge_chart):
                    image = add_BB(image, track_id_to_print, txt_lines[i].x_min, txt_lines[i].x_max,
                                   txt_lines[i].y_min, txt_lines[i].y_max, "black", main_track_bol, font_size)

    return image


def add_BB(image, track_id, x1, x2, y1, y2, color, main_track_bol, font_size):
    text_x1 = 0
    text_y1 = 0
    location =[]
    try:

        if int(x1) < 100:       ##dont draw outside pic
            text_x1 = int(x2) + 17
        else:
            text_x1 = int(x1) - 17

        if int(y1) < 20:  ##dont draw outside pic
            text_y1 = int(y1) + 17
        else:
            text_y1 = int(y1)

        try:
            font = ImageFont.truetype("C:\Windows\Fonts\Arial.ttf", font_size)
        except:
            logging.warning("font not found: C:\Windows\Fonts\Arial.ttf")

        draw = ImageDraw.Draw(image)

        draw.text((text_x1, text_y1), str(track_id), font=font, fill=color)
        location = [int(x1), int(y1), int(x2), int(y2)]

        bold_bb = 3
        if font_size < 20:
            bold_bb = 1
        if main_track_bol:
            for i in range(bold_bb):  #bold bb
                draw.rectangle(location, outline = color)
                location = (location[0] + 1, location[1] + 1, location[2] + 1, location[3] + 1)
        else:
            draw.rectangle(location, outline=color)

    except:
        logging.warning("add_BB: saving image for track_id (%s) with BB failed ", str(track_id))
        logging.warning("textX1: %s textY1: %s color: %s location: %s" %(text_x1, text_y1, color, location))
        time.sleep(0.1)
        return image
    return image

import sys

from csv_handler import create_user_log_file
from server_handler import check_if_file_exist
import ConfigParser
config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

path_user_log_files = config.get('paths', 'path_user_log_files')


def log_in_user(ssh):

    user = str(raw_input("enter user name "))
    file_path = path_user_log_files + "/" + user + ".csv"
    if not check_if_file_exist(ssh, file_path):
        answer = query_yes_no("user name does not exist, create new one?")
        if answer:
            create_user_log_file(ssh, user)
        else:
            return log_in_user(ssh)

    return user

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
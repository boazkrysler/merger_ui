class Track:
    def __init__(self, track_id, label, attributes, track_start_segment):
        self.track_id = track_id
        self.label = label
        self.attributes = attributes
        self.track_start_segment = track_start_segment

    def print_track(self):
        print "Track: track_id: ", self.track_id, " label: " , self.label, " start_segment: ", self.track_start_segment, " attributes: " , self.attributes
    def get_track(self):
        txt = "Track: track_id: ", self.track_id, " label: " , self.label, " start_segment: ", self.track_start_segment, " attributes: " , self.attributes
        return txt



class Line:
    # The class "constructor" - It  's actually an initializer
    def __init__(self, track_id, x_min, y_min, x_max, y_max, frame, lost, occluded, generated, label, attributes, track_start_segment):
        self.track_id = track_id
        self.x_min = x_min
        self.y_min = y_min
        self.x_max = x_max
        self.y_max = y_max
        self.frame = frame
        self.lost = lost
        self.occluded = occluded
        self.generated = generated
        self.label = label
        self.attributes = attributes
        self.track_start_segment = track_start_segment

    def print_line(self):
        print "Line: track_id: ", self.track_id, " label: " , self.label, " start_segment: ", self.track_start_segment, " frame:" , self.frame
    def get_line(self):
        line = "Line: track_id: " + str(self.track_id) + " label: " + str(self.label) + " start_segment: " + str(self.track_start_segment) + " frame:" + str(self.frame)
        return line

    def get_turkic_line_str(self):
        return str(self.track_id) + " " + str(self.x_min) + " " + str(self.y_min) + " " + str(self.x_max) + " " + str(self.y_max) \
               + " " + str(self.frame) + " " + str(self.lost) + " " + str(self.occluded) \
               + " " + str(self.generated) + " " + str(self.label) + " " + str(self.attributes).strip('[]').translate(None, "'")

def make_Line(track_id, x_min, y_min, x_max, y_max, frame, lost, occluded, generated, label, attributes, segment):
    return Line(track_id, x_min, y_min, x_max, y_max, frame, lost, occluded, generated, label, attributes, segment)

import paramiko
import logging
from sys import exit

import ConfigParser
config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

target_host = config.get('server', 'ip')
pwd = config.get('server', 'password')
user_name = config.get('server', 'user_name')

def connect_to_server():
    ssh = paramiko.SSHClient()
    #key = paramiko.RSAKey.from_private_key_file("C:\\Users\\Boaz\\PycharmProjects\\findDiffrentAttributes\\id_rsa_boaz")
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    '''target_host = '169.55.111.170'
    target_port = 22
    pwd = 'ops123!'
    user_name = 'ops'
    '''
    #ssh.connect(hostname=target_host, username=un, password=pwd ,pkey = key)
    ssh.connect(hostname=target_host, username=user_name, password=pwd)
    return ssh


def check_if_file_exist(ssh, path):
    sftp = ssh.open_sftp()
    try:
        sftp.stat(path)
        return True
    except:
        return False
    sftp.close()

def copy_file_to_server(ssh, path, path_on_server):
    sftp = ssh.open_sftp()
    sftp.put(path, path_on_server)
    sftp.close()


def open_file_on_server(ssh, remote_dir, file_name):
    sftp = ssh.open_sftp()
    dir_items = sftp.listdir_attr(remote_dir)
    for item in dir_items:
        # assuming the local system is Windows and the remote system is Linux
        # os.path.join won't help here, so construct remote_path manually
        remote_path = remote_dir + '/' + item.filename
        if item.filename == file_name:
            file_to_get = sftp.open(remote_path)
            logging.info("open_file_on_server: file_to_read: %s" %item.filename)
            return file_to_get

    logging.warning("open_file_on_server: no file found. dir: %s  file: %s" %(remote_dir, file_name))
    exit()


def run_command_on_server(ssh, command):
    stdin, stdout, stderr = ssh.exec_command(command)
    if len(stderr.read()) > 2:
        logging.warning("STDOUT:\n%s\n\nSTDERR:\n%s\n" % (stdout.read(), stderr.read()))
    else:
        logging.debug("stdout: %s" %stdout.read())
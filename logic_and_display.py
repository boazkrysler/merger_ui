import os
import tkMessageBox
from datetime import datetime
from Tkinter import *
from tkFont import Font
from sys import exit

from PIL import Image, ImageTk
import ConfigParser
import logging

from PIL import ImageFont
from tkinter import ttk

from cmd import merge_video, delete_temp_files, copy_file
from csv_handler import create_merger_file, add_user_info, add_video_finished_to_chart
from image_handler import get_image_path, draw_on_image
from server_handler import copy_file_to_server, open_file_on_server


config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))

path_images_on_storage = config.get('paths', 'path_images_on_storage')
path_merger_file = config.get('paths', 'path_merger_file')
path_temp_files_on_windows = config.get('paths', 'path_temp_files_on_windows')

path_save_finish_chart_on_storage = config.get('paths', 'path_save_finish_chart_on_storage')


def get_merge_chart():
    return merge_chart

ssh = ""
merge_chart = ""
video_name = ""
txt_lines = []
local_image_path = ""
max_frame = 0
edit_mode_bol = False
unmerged_mode_bol = False
track_id_working = -1
same_img_bol = False
video_path =""
delete_files_bol = False
user = ""
complexity = 0
time_start = 0
current_label = ""
def set_up_presentation(ssh_temp, txt_lines_tmp, merge_chart_tmp, video_name_tmp,
                        local_image_path_tmp, video_path_tmp, user_tmp, complexity_tmp, segment_length_tmp, gap_tmp):
    global ssh, txt_lines, merge_chart, video_name, local_image_path, max_frame, video_path, track_id_working, \
        user, complexity, segment_length, gap,segment_length_minus_gap
    ssh = ssh_temp
    merge_chart = merge_chart_tmp.read().replace("\r","")
    merge_chart = merge_chart.split('\n')
    for i in range(len(merge_chart)):
        merge_chart[i] = merge_chart[i].split(',')

    video_name = video_name_tmp
    txt_lines = txt_lines_tmp
    local_image_path = local_image_path_tmp
    max_frame = int(txt_lines[len(txt_lines) - 1].frame)
    video_path = video_path_tmp
    '''try:
        os.system(video_path)
    except:
        logging.debug("video does not exist: " % video_path)
    '''
    track_id_working, bol = find_next_track_id(-1)
    find_label()
    user = user_tmp
    complexity = complexity_tmp
    gap = gap_tmp
    segment_length = segment_length_tmp

gap = 21
segment_length = 321
start_frame = 1
jump_frame = 10
play_speed = 1000
def run_presentation(master = 0):
    if master != 0:
        master.destroy()

    find_label()
    main_screen()
    return

def get_delete_files_bol():
    return delete_files_bol


def start_time():
    global time_start
    if time_start == 0:
        time_start = datetime.now()

def save_chart(master, charts, run_bol):
    global merge_chart

    charts_text = []
    for i in range(len(charts)):
        charts_text.append(charts[i].get("1.0", END).split("\n"))

    new_merge_chart = []
    for row in range(len(charts_text[0])):
        line = []
        for chart in charts_text:
            line.append(chart[row])
        add = False
        for index in line:
            if index!="":
                add = True
                break
        if add:
            new_merge_chart.append(line)

    for i in range(len(new_merge_chart)):
        for j in range(len(new_merge_chart[i])):
            if new_merge_chart[i][j] == "":
                for h in range(len(new_merge_chart[i])):
                    if new_merge_chart[i][h] != "":
                        if h < j:
                            new_merge_chart[i][j]="-8"
                        else:
                            new_merge_chart[i][j] = "-5"

    '''
    for i in range(1,len(b)):
        line = []
        for j in range(len(b[i])):
            if str(b[i][j].get()) != "":
                line.append(str(b[i][j].get()))
            else:
                add = False
                for g in range(j, len(b[i])):
                    if str(b[i][g].get()) != "":
                        add = True
                if add:
                    line.append(-5)
        if len(line) > 0:
            new_merge_chart.append(line)
    '''
    for j in range(len(new_merge_chart) * len(new_merge_chart[0])):
        count = 0
        for line in new_merge_chart:
            for number in line:
                try:
                    if number == str(j):
                        count += 1
                except:
                    pass
        if count > 1:
            line = "Error - track id %s appear more then once. chart was not saved" %j
            tkMessageBox.showinfo("chart not saved", line)
            return False



    merge_chart = new_merge_chart
    file_path = create_merger_file(video_name, merge_chart)
    copy_file_to_server(ssh, file_path, path_merger_file + "/" + video_name + ".csv")

    find_prev_track_id()
    if run_bol:
        run_presentation(master=master)
    return

play = False
video_finished = False
job =""

class ScrolledTextPair(Frame):
    '''Two Text widgets and a Scrollbar in a Frame'''

    def __init__(self, master, number_of_segments, **kwargs):
        Frame.__init__(self, master) # no need for super

        # Different default width
        if 'width' not in kwargs:
            kwargs['width'] = 7

        # Creating the widgets
        self.charts = [None] * number_of_segments
        for i in range(number_of_segments):
            self.charts[i] = Text(self, **kwargs)
            self.charts[i].pack(side=LEFT, fill=BOTH)

        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        # Changing the settings to make the scrolling work
        self.scrollbar['command'] = self.on_scrollbar
        for i in range(number_of_segments):
            self.charts[i]['yscrollcommand'] = self.on_textscroll

    def on_scrollbar(self, *args):
        '''Scrolls both text widgets when the scrollbar is moved'''
        for i in range(len(self.charts)):
            self.charts[i].yview(*args)

    def on_textscroll(self, *args):
        '''Moves the scrollbar and scrolls text widgets when the mousewheel
        is moved on a text widget'''
        self.scrollbar.set(*args)
        self.on_scrollbar('moveto', args[0])

def find_label():
    global current_label
    for line in txt_lines:
        if int(line.track_id) == track_id_working:
            current_label = line.label
            return


row_start_line = 0
chart_start_segment = 0
def main_screen():
    global play

    master = PanedWindow(orient=VERTICAL)
    master.pack(fill=BOTH, expand=1)


    top = PanedWindow(master)
    master.add(top)

    # segments head:
    myFont = Font(size=14, underline = True)
    segment_label_1 = Label(top, text="segment 1", width = 50, font = myFont)
    top.add(segment_label_1)
    segment_label_2 = Label(top, text="segment 1", width = 50, font = myFont)
    top.add(segment_label_2)
    segment_label_3 = Label(top, text="segment 1", width = 50, font = myFont)
    top.add(segment_label_3)


    middle = PanedWindow(master)
    master.add(middle)

    #scrollbar = Scrollbar(middle)
    #scrollbar.pack(side=RIGHT, fill=Y)

    buttons_line = PanedWindow(master, orient= HORIZONTAL)
    master.add(buttons_line)

    #chart_buttons_line = PanedWindow(master, orient= HORIZONTAL)
    #master.add(chart_buttons_line)

    ##------chart---------
    csv_info = list(merge_chart)
    header = csv_info[0]
    #header = header[chart_start_segment:]
    colors = ["blue","green","purple", "black"]
    color_counter = 0
    chart_line = PanedWindow(master, orient=HORIZONTAL)
    master.add(chart_line)

    number_of_segments = len(csv_info[0])
    text_charts_container = ScrolledTextPair(chart_line, number_of_segments, bg='white', fg='black')
    text_charts_container.pack(fill=BOTH, expand=True)

    for i in range(len(csv_info) + 2):  # Rows
        try:
            line_info = csv_info[i]
        except:
            line_info = [""] * len(header)
        for j in range(len(header)):  # Columns
            if i == 0:
                seg = "S_" + str(j + 1)
                text_charts_container.charts[j].insert(INSERT, seg + "\n")
            else:
                try:
                    if str(track_id_working) in line_info:
                        if len(line_info) > j:
                            text_charts_container.charts[j].insert(INSERT, line_info[j] + "\n", 'red')
                        else:
                            text_charts_container.charts[j].insert(INSERT, "-8" + "\n", 'red')
                    else:
                        if len(line_info) > j:
                            text_charts_container.charts[j].insert(INSERT, line_info[j] + "\n", colors[color_counter%len(colors)])
                        else:
                            text_charts_container.charts[j].insert(INSERT, "-8" + "\n", colors[color_counter%len(colors)])
                except:
                    print("error") #delete me
                    pass
        color_counter += 1
        #chart[j].insert(INSERT,"\n ")

    for ch in text_charts_container.charts:
        ch.tag_config('red', foreground='red')

        for j in range(len(colors)):
          ch.tag_config(colors[j], foreground=colors[j])


    img_frame = start_frame
    image_1, end_of_video = get_image_path(max_frame, video_name, img_frame)
    image_1 = open_and_paint_image(image_1, img_frame)

    img_frame = start_frame + jump_frame
    image_2, end_of_video = get_image_path(max_frame, video_name, img_frame)
    image_2 = open_and_paint_image(image_2, img_frame)

    img_frame = start_frame + jump_frame * 2
    image_3, end_of_video = get_image_path(max_frame, video_name, img_frame)
    image_3 = open_and_paint_image(image_3, img_frame)

    pic_1 = Label(middle, image=image_1)
    middle.add(pic_1)
    pic_2 = Label(middle, image=image_2)
    middle.add(pic_2)
    pic_3 = Label(middle, image=image_3)
    middle.add(pic_3)

    def save_chart_command():
        save_chart(master, text_charts_container.charts, True)

    def next_seg_command():
        global start_frame

        segment_num = start_frame / (segment_length-21) + 1

        start_frame = (segment_length - 21) * segment_num + 1
        if start_frame < 0:
            start_frame = 1
        next_image_command()

    def prev_button_command():
        global start_frame
        start_frame -= jump_frame * 3
        if start_frame < 0:
            start_frame = 1
        next_image_command()

    def next_image_command():
        global same_img_bol
        start_time()
        if not same_img_bol:
            next_image(jump_frames_entry)

        same_img_bol = False

        img_frame = start_frame
        new_image, end_of_video = get_image_path(max_frame, video_name, img_frame)
        new_image = open_and_paint_image(new_image, img_frame)
        pic_1.configure(image=new_image)
        pic_1.image = new_image

        if end_of_video and play:
            cancel()
            next_id(master)
        try:
            img_frame = start_frame + jump_frame
            new_image, end_of_video = get_image_path(max_frame, video_name, img_frame)
            new_image = open_and_paint_image(new_image, img_frame)
            pic_2.configure(image=new_image)
            pic_2.image = new_image

            img_frame = start_frame + jump_frame * 2
            new_image, end_of_video = get_image_path(max_frame, video_name, img_frame)
            new_image = open_and_paint_image(new_image, img_frame)
            pic_3.configure(image=new_image)
            pic_3.image = new_image

            if end_of_video and not play:
                tkMessageBox.showinfo("end frame", "showing last frame")

            ##headers - segment number
            segment_number = "segment " + str(int(start_frame) / segment_length + 1)
            if int(start_frame + jump_frame) / segment_length + 1 > len(merge_chart[0]):
                segment_number = "segment " + str(int(start_frame) / segment_length + 1)
            segment_label_1.config(text=segment_number)
            segment_number = "segment " + str(int(start_frame + jump_frame) / segment_length + 1)
            if int(start_frame + jump_frame) / segment_length + 1 > len(merge_chart[0]):
                segment_number = "segment " + str(int(start_frame + jump_frame) / segment_length + 1)
            segment_label_2.config(text=segment_number)
            segment_number = "segment " + str(int(start_frame + jump_frame * 2) / segment_length + 1)
            if int(start_frame + jump_frame) / segment_length + 1 > len(merge_chart[0]):
                segment_number = "segment " + str(int(start_frame + jump_frame * 2) / segment_length + 1)
            segment_label_3.config(text=segment_number)
        except:
            print " "

    def prev_id_command():
        start_time()
        prev_id(master)

    def next_10_id_command():
        start_time()
        next_id(master, jump=10)

    def next_id_command():
        start_time()
        next_id(master)


    def edit_button_command():
        global edit_mode_bol, same_img_bol, unmerged_mode_bol
        start_time()
        unmerged_mode_bol = False
        edit_mode_bol = not edit_mode_bol
        same_img_bol = True
        next_image_command()


    def full_screen_command():
        from win32api import GetSystemMetrics
        width = GetSystemMetrics(0) - 100
        height = GetSystemMetrics(1) - 100
        novi = Toplevel()
        canvas = Canvas(novi, width=width, height=height)
        canvas.pack(expand=YES, fill=BOTH)
        #gif1 = PhotoImage(file='image.gif')
        # image not visual

        image_full_screen, temp = get_image_path(max_frame, video_name, start_frame)
        font_size = 12
        image_full_screen = open_and_paint_image(image_full_screen, start_frame, width, font_size)

        canvas.create_image(50, 10, image=image_full_screen, anchor=NW)
        # assigned the gif1 to the canvas object
        canvas.gif1 = image_full_screen
        pass


    def unmerged_button_command():
        global unmerged_mode_bol, same_img_bol, edit_mode_bol
        start_time()
        edit_mode_bol = False
        unmerged_mode_bol = not unmerged_mode_bol
        same_img_bol = True
        next_image_command()

    def start_over_button_command():
        global edit_mode_bol, same_img_bol, track_id_working, start_frame
        edit_mode_bol = False
        same_img_bol = False
        start_frame = 1
        track_id_working, bol = find_next_track_id(-1)
        run_presentation(master)

    def play_button_impl():
        global job
        update_play_speed(play_speed_entry)
        next_image_command()
        if play:
            job = master.after(play_speed, play_button_impl)

    def play_button():
        global play
        start_time()
        play = not play
        if play:
            play_b.config(relief=SUNKEN)
            play_button_impl()
        if not play:
            play_b.config(relief=RAISED)

            cancel()

    def cancel():
        global job
        if job is not None:
            master.after_cancel(job)
            job = None

    def exit_and_del_command():
        global delete_files_bol, video_finished
        save_chart(master, text_charts_container.charts, False)
        delete_files_bol = True
        video_finished = True
        add_video_finished_to_chart(ssh, video_name)
        copy_file(ssh, path_merger_file + "/" + video_name + ".csv", path_save_finish_chart_on_storage)
        print "deleting temp files"
        local_temp_files = os.getcwd() + path_temp_files_on_windows
        delete_temp_files(local_temp_files + "/" + video_name)
        os.remove(local_temp_files + "/" + video_name + ".mp4")
        save_and_exit()
        master.quit()
        exit()

    def save_and_exit():
        print "good work"
        cancel()  # stop play
        save_chart(master, text_charts_container.charts, False)
        try:
            work_time = datetime.now() - time_start
        except: # time_start != time. user saving without working
            work_time = 0
        add_user_info(ssh, user, video_name, complexity, work_time, video_finished)

        my_font = Font(size=80, underline=True)
        loading = Label(master, text="Exiting...", width=50, font=my_font)
        master.add(loading)
        master.update()
        master.quit()
        exit()


    def merge_video_button():
        global merge_chart
        merge_video(ssh, video_name, True)
        merge_chart_tmp = open_file_on_server(ssh, path_merger_file, video_name + ".csv")
        merge_chart = merge_chart_tmp.read().replace("\r", "")
        merge_chart = merge_chart.split('\n')
        for i in range(len(merge_chart)):
            merge_chart[i] = merge_chart[i].split(',')

        run_presentation(master)

    next_seg_button = Button(buttons_line, text="Next Segment", command=next_seg_command)
    buttons_line.add(next_seg_button)

    next_button = Button(buttons_line, text="Next", command=next_image_command)
    buttons_line.add(next_button)

    prev_button = Button(buttons_line, text="Prev", command=prev_button_command)
    buttons_line.add(prev_button)

    var = StringVar()
    label = Label(buttons_line, textvariable=var, relief=RAISED)
    var.set("Number of frames to jump: ")
    buttons_line.add(label)

    jump_frames_entry = Entry(buttons_line, bd=5)
    jump_frames_entry.insert(0, jump_frame)
    buttons_line.add(jump_frames_entry)


    var = StringVar()
    label = Label(buttons_line, textvariable=var, relief=RAISED)
    var.set("Play speed (seconds):")
    buttons_line.add(label)

    play_speed_entry = Entry(buttons_line, bd=5)
    play_speed_entry.insert(0, jump_frame)
    buttons_line.add(play_speed_entry)

    next_id_button = Button(buttons_line, text="Next_ID", command=next_id_command)
    buttons_line.add(next_id_button)

    next_10_id_button = Button(buttons_line, text="Next_10_ID", command=next_10_id_command)
    buttons_line.add(next_10_id_button)

    prev_id_button = Button(buttons_line, text="Prev_ID", command=prev_id_command)
    buttons_line.add(prev_id_button)

    save_and_exit_button = Button(buttons_line, text="Save and exit", command=save_and_exit)
    buttons_line.add(save_and_exit_button)

    edit_button = Button(buttons_line, text="Edit view", command=edit_button_command)
    buttons_line.add(edit_button)

    full_screen_button = Button(buttons_line, text="Full screen", command=full_screen_command)
    buttons_line.add(full_screen_button)

    unmerged_button = Button(buttons_line, text="Unmerged view", command=unmerged_button_command)
    buttons_line.add(unmerged_button)

    start_over_button = Button(buttons_line, text="Go to top", command=start_over_button_command)
    buttons_line.add(start_over_button)

    save_button = Button(buttons_line, text="Save chart", command=save_chart_command)
    buttons_line.add(save_button)

    merge_button = Button(buttons_line, text="Remake chart", command=merge_video_button)
    buttons_line.add(merge_button)

    exit_and_del = Button(buttons_line, text="Video finished, Save, Merge, Exit and Delete images", command=exit_and_del_command)
    buttons_line.add(exit_and_del)

    play_b = Button(buttons_line, text="Play", command=play_button)
    buttons_line.add(play_b)

    var = StringVar()
    label = Label(buttons_line, textvariable=var, relief=RAISED)
    var.set("Label: " + current_label)
    buttons_line.add(label)

    if play:
        play_b.config(relief=SUNKEN)
        play_button_impl()
    mainloop()
    return master



def image_resize(img, base_width):
    wpercent = (base_width / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    return img.resize((base_width, hsize), Image.ANTIALIAS)


def open_and_paint_image(image, frame, base_width = 600, font_size = 50):
    image = Image.open(image, 'r')
    image = draw_on_image(image, track_id_working, txt_lines, frame, merge_chart, edit_mode_bol, unmerged_mode_bol, font_size)
    image = image_resize(image, base_width)
    return ImageTk.PhotoImage(image)


def csv_info_searcher(csv_info, row, col):
    for i in range(row + 1, len(csv_info) + 2):  # Rows
        for j in range(len(csv_info[0])):  # Columns
            if int(csv_info[i][j]) != -5:
                return int(csv_info[i][j])


def find_prev_track_id():
    global track_id_working
    csv_info = list(merge_chart)

    header = csv_info[0]
    for i in range(1, len(csv_info) + 2):  # Rows
        try:
            line_info = csv_info[i]
        except:
            line_info = ("", "")
        for j in range(len(header)):  # Columns
            try:
                if line_info[j] == str(track_id_working):
                    if i + 1 < len(csv_info):

                        track_id_working = csv_info_searcher(csv_info, i-2, j)
                        return track_id_working
            except:
                break
    track_id_working = csv_info_searcher(csv_info, 0, 0)
    return track_id_working


def find_next_track_id(current_track_id):
    global track_id_working
    csv_info = list(merge_chart)

    if current_track_id == -1:
        track_id_working = csv_info_searcher(csv_info, 0, 0)
        return track_id_working, False

    header = csv_info[0]
    for i in range(len(csv_info) + 2):  # Rows
        try:
            line_info = csv_info[i]
        except:
            line_info = ("", "")
        for j in range(len(header)):  # Columns
            try:
                if line_info[j] == str(track_id_working):
                    if i+1 < len(csv_info):
                        track_id_working = csv_info_searcher(csv_info,i,j)
                        return track_id_working, False
            except:
                break
    track_id_working = csv_info_searcher(csv_info,0,0)
    start_over = True
    return track_id_working, start_over


def prev_id(master):
    global track_id_working, start_frame, edit_mode_bol, play
    track_id_working = find_prev_track_id()

    start_frame = 1
    edit_mode_bol = False
    run_presentation(master=master)

def next_id(master, jump = 1):
    global track_id_working, start_frame, edit_mode_bol,play
    for i in range(jump):   #FIX
        track_id_working, start_over = find_next_track_id(track_id_working)
    if start_over:
        play = False
    start_frame = 1
    edit_mode_bol = False
    run_presentation(master=master)


def update_play_speed(play_entry):
    global play_speed
    try:
        play_speed = int(play_entry.get())
        if play_speed < 1:
            play_speed = 1000
    except:
        tkMessageBox.showinfo("error msg", "enter a valid number to play_speed")
        play_entry.delete(0,"end")
        play_entry.insert(0,1000)
        play_speed = 1000


def next_image(jump_frames_entry):
    global jump_frame, start_frame
    last_jump_frame = jump_frame
    try:
        jump_frame = int(jump_frames_entry.get())
        if jump_frame < 1:
            jump_frame = 10
    except:
        tkMessageBox.showinfo("error msg", "enter a valid number to jump_frame")
        jump_frame = 10
        return

    if last_jump_frame == jump_frame:
        start_frame += jump_frame
        #no wait. just switch pic 1 = last pix_2 ...
        '''    run_presentation(master=master, last_image_2= image_2, last_image_3= image_3)

    run_presentation(master=master)
    '''
    return

def image_switch():
    root = Tk()
    path = "C:\Users\Boaz\PycharmProjects\merger_UI\\1.jpg"
    path2 = "C:\Users\Boaz\PycharmProjects\merger_UI\\2.jpg"

    img = ImageTk.PhotoImage(Image.open(path))
    panel = Label(root, image=img)
    panel.pack(side="bottom", fill="both", expand="yes")

    def callback(e):
        img2 = ImageTk.PhotoImage(Image.open(path2))
        panel.configure(image=img2)
        panel.image = img2

    root.bind("<Return>", callback)
    root.mainloop()
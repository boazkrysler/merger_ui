import MySQLdb
from sshtunnel import SSHTunnelForwarder
import logging


def get_complexity(video_name):
    command = 'select COUNT(DISTINCT p.id) as "paths_sum", COUNT(DISTINCT b.id) as "boxes_sum" from videos v join segments s on v.id=s.videoid join jobs j  on j.segmentid=s.id join paths p on j.id=p.jobid join boxes b on p.id=b.pathid where slug="' + video_name + '";'
    with SSHTunnelForwarder(
            ('169.55.111.170', 22),
            ssh_password="ops123!",
            ssh_username="ops",
            remote_bind_address=('localhost', 3306)) as server:
        db = MySQLdb.connect(host='127.0.0.1',
                             port=server.local_bind_port,
                             user='root',
                             passwd='viisights5344',
                             db='vatic',
                             connect_timeout=50)
        db.query(command)

    data = db.store_result()
    data = data.fetch_row(0)
    paths_count = data[0][0]
    boxes_count = data[0][1]
    db.close()
    if paths_count == float(0):
        complexity = 1
    else:
        complexity = float(boxes_count) / float(paths_count)

    return complexity


def find_segment_length_and_gap(video_name):

    command = "select start, stop from videos v join segments s on v.id=s.videoid where slug='" + video_name + "';"

    with SSHTunnelForwarder(
            ('169.55.111.170', 22),
            ssh_password="ops123!",
            ssh_username="ops",
            remote_bind_address=('localhost', 3306)) as server:
        db = MySQLdb.connect(host='127.0.0.1',
                             port=server.local_bind_port,
                             user='root',
                             passwd='viisights5344',
                             db='vatic',
                             connect_timeout=50)
        db.query(command)

    data = db.store_result()
    data = data.fetch_row(0)

    data_list = list(data)

    try:
        gap = int(data_list[1][0]) - int(data_list[0][1])
    except:
        gap = 21    # only 1 segment
    logging.debug("find_segment_length: sql command: %s", command)
    logging.debug("length returned: " + str(data_list[0]))

    db.close()
    return int(data_list[0][1]), gap


